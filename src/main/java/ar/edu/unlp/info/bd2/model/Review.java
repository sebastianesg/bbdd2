package ar.edu.unlp.info.bd2.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Review {
    @Id
    @Column(name="id")@GeneratedValue
    public Integer id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "branch", referencedColumnName = "id")
    Branch branch;

   /* @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user", referencedColumnName = "id")
    User user;*/

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "author", referencedColumnName = "id")
    public User author;

    @OneToMany
    public List<FileReview> fileReviews= new ArrayList<FileReview>();

    public Review(Branch branch, User user) {
        this.branch = branch;
        this.author = user;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<FileReview> getFileReviews() {
        return fileReviews;
    }
    public Review addFileReviews(FileReview fw) {
        this.fileReviews.add(fw);
        return this;
    }

    public void setFileReviews(List<FileReview> fileReviews) {
        this.fileReviews = fileReviews;
    }

    public List<FileReview> getReviews(){

        return this.getFileReviews();
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User user) {
        this.author = user;
    }
}
