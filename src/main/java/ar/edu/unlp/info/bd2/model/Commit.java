package ar.edu.unlp.info.bd2.model;
import javax.persistence.*;


import java.util.*;

@Entity
public class Commit {
    @Id
    @Column(name="id")@GeneratedValue
    public Integer id;

    @Column(name="descripcion")
    public String descripcion;

    @Column(name="hash")
    public String hash;

    @OneToMany
    public List<File> files = new ArrayList<File>();

    @ManyToOne
    @JoinColumn(name="branch_id",referencedColumnName="id")
    public Branch branch;

    @ManyToOne
    @JoinColumn(name="user_id",referencedColumnName="id")
    public User user;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "author", referencedColumnName = "id")
    public User author;

    public Tag getUnTag() {
        return unTag;
    }

    public void setUnTag(Tag unTag) {
        this.unTag = unTag;
    }

    /********************************************************************/
    /* TAG muchos Commits a un TAG  */
    /*@ManyToOne
    @JoinColumn (name = "tag", referencedColumnName = "id")
    public Tag tag;*/

    /* TAG un commit con muchos TAG */
    /*@OneToMany
    public List<Tag> tags = new ArrayList<Tag>();*/

    /* TAG: un commit con un TAG  */
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "tag_id", referencedColumnName = "id")
    public Tag unTag;

    /********************************************************************/

    public Commit(String descripcion, String hash, User author, List<File> files, Branch branch) {
        this.descripcion = descripcion;
        this.hash = hash;
        this.author = author;
        this.branch=branch;
    }

    public void addFile (File file){
        this.files.add(file);
        file.setCommit(this);
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setFiles(List<File> files) {
        /*this.files = files;*/
    }

    public String getDescription() {
       /* return description;*/
        return "";
    }

    public void setDescription(String description) {
       /* this.description = description;*/
    }

    public void setAuthor(User author) {
       /* this.author = author;*/
    }

    public void setId(Integer id){
        this.id = id;
    }

    public void setMessage(String description){
        descripcion = description;
    }

    public void setHash(String hash){

        this.hash = hash;
    }

    public Integer getId(){
        return id;
    }

    public String getMessage() {
        return descripcion;
    }

    public String getHash() {
        return hash;
    }

    public User getAuthor() {
        return author;
    }
    public Branch getBranch() {
        return branch;
    }

    public List<File> getFiles() {
        return files;
    }
}
