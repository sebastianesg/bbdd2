package ar.edu.unlp.info.bd2.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.*;

@Entity

public class File {
    @Id
    @Column(name="id")@GeneratedValue
    public Integer id;
    @Column(name="nombre")
    public String nombre;
    @Column(name="contenido")
    public String contenido;
    @ManyToOne
    @JoinColumn(name="commit_id",referencedColumnName="id")
    public Commit commit;

    public File(String nombre, String contenido) {
        this.nombre = nombre;
        this.contenido = contenido;
    }

    public void setId (Integer id) {

        this.id = id;
    }

    public void setName(String name){

        nombre = name;
    }

    public void setContent(String content){
        contenido = content;
    }

    public Integer getId() {

        return id;
    }


    public String getFilename(){
        return nombre;
    }

    public String getContent() {
        return contenido;
    }

    public void setCommit(Commit commit) {
        this.commit = commit;
    }
    public Commit getCommit() {
        return commit;
    }
}

