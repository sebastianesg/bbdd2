package ar.edu.unlp.info.bd2.model;

import javax.persistence.*;

@Entity
public class FileReview {
    @Id
    @Column(name="id")@GeneratedValue
    public Integer id;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "review", referencedColumnName = "id")
    public Review review;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "file", referencedColumnName = "id")
    public File reviewedFile;
    @Column(name="comment")
    public String comment;
    @Column(name="line")
    public Integer line;

    public FileReview(Review review, File file, String comment, int line) {
        this.review = review;
        this.reviewedFile = file;
        this.comment = comment;
        this.line = line;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Review getReview() {
        return review;
    }

    public void setReview(Review review) {
        this.review = review;
    }

    public File getReviewedFile() {
        return reviewedFile;
    }

    public void setReviewedFile(File file) {
        this.reviewedFile = file;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getLine() {
        return line;
    }

    public void setLineNumber(Integer line) {
        this.line = line;
    }

    public Integer getLineNumber(){
        return this.line;
    }
}
