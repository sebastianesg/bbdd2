package ar.edu.unlp.info.bd2.model;


import javax.persistence.*;

@Entity
public class Tag {
    @Id
    @Column(name = "id")
    @GeneratedValue
    public Integer id;

    @Column(name = "nombreTag")
    public String tagName;

    /*@OneToOne
    @JoinColumn(name = "commit_id", referencedColumnName = "id")*/
    //public String hashCommit;  no va mas

    /*public Tag(String hashCommit, String tagName) {
        //this.hashCommit = hashCommit;
        this.tagName = tagName;
    }*/

    public Tag(String tagName) {
        this.tagName = tagName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {

        return tagName;
    }

    public void setName(String tagName) {

        this.tagName = tagName;
    }

    /*public void setCommit(String commit) {
        this.hashCommit = commit;
    }*/
}
