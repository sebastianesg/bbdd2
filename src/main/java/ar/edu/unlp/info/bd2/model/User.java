package ar.edu.unlp.info.bd2.model;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class User {
    @Id
    @Column(name="id")@GeneratedValue
    public Long id;
    @Column(name="nombre")
    public String nombre;
    @Column(name="email")
    public String email;
    @OneToMany
    public List<Commit> commits= new ArrayList<Commit>();

    public User(String email, String nombre) {
        this.setName(nombre);
        this.setEmail(email);

    }
    public void addCommit(Commit commit) {
        this.commits.add(commit);
    }
    public void setId (Long id) {
        this.id = id;
    }

    public void setName(String name){
        nombre = name;
    }

    public void setEmail(String email){
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public String getName(){
        return nombre;
    }

    public String getEmail(){
        return email;
    }

}

