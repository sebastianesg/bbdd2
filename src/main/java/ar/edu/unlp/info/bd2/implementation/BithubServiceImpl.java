package ar.edu.unlp.info.bd2.implementation;

import ar.edu.unlp.info.bd2.model.*;


import ar.edu.unlp.info.bd2.repositories.BithubRepository;
import ar.edu.unlp.info.bd2.services.BithubException;
import ar.edu.unlp.info.bd2.services.BithubService;

import javax.persistence.NoResultException;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
//import org.springframework.test.annotation.Commit;

public  class BithubServiceImpl implements BithubService {

    private BithubRepository repository;

    @org.jetbrains.annotations.Contract(pure = true)
    public BithubServiceImpl (BithubRepository repository){this.repository = repository;}

    @Override
    public User createUser(String email, String name) {
        User usuario = new User(email,name);
        repository.addUser(usuario);
        return usuario;
    }

    @Override
    public Optional<User> getUserByEmail(String email) {
        return Optional.empty();
    }

    @Override
    public Branch createBranch(String name) {
        Branch branch = new Branch(name);
        repository.addBranch(branch);
        return branch;
    }

    @Override
    public Commit createCommit(String description, String hash, User author, List<File> files, Branch branch) {
        Commit commit = new Commit(description,hash,author,files,branch);
        repository.addCommit(commit);
        branch.addCommit(commit);
        repository.updateBranch(branch);
        files.forEach(n->commit.addFile(n));
        files.forEach(n->repository.updateFile(n));
        author.addCommit(commit);
        repository.updateUser(author);
        return commit;
    }

    @Override
    public Tag createTagForCommit(String commitHash, String name) throws BithubException {

            //recuperamos el commit
            Commit unCommit = repository.getCommitByHash(commitHash);
            if (unCommit==null) {
                throw new  BithubException("error");
            }else{
            //creamos el tag y asociamos el nombre
            Tag nuevoTag = new Tag(name);
            repository.addTag(nuevoTag);
            //asociamos el tag al commit recuperado
            unCommit.setUnTag(nuevoTag);
            //guardamos los cambios
            repository.updateCommit(unCommit);
            return nuevoTag;

        }

    }

    @Override
    public Optional<Commit> getCommitByHash(String commitHash)
    {
        Commit commit=repository.getCommitByHash(commitHash);
        return Optional.ofNullable(commit);
    }

    @Override
    public File createFile(String name, String content) {
        File file = new File(content,name);
        repository.addFile(file);
        return file;
    }

    @Override
    public Optional<Tag> getTagByName(String tagName){
        return Optional.ofNullable(repository.getTagByName(tagName));
    }

    @Override
    public Review createReview(Branch branch, User user) {

        Review review = new Review(branch,user);
        repository.addReview(review);
        return review;
    }

    @Override
    public FileReview addFileReview(Review review, File file, int lineNumber, String comment) throws BithubException {
        try {
            //creamos el fileReview
            if (file.getCommit().getBranch()==review.getBranch()){
                FileReview nuevoFileRev = new FileReview(review, file, comment, lineNumber);
                repository.addFileReview(nuevoFileRev);
                review.addFileReviews(nuevoFileRev);
                repository.updateReview(review);
                return nuevoFileRev;
            }else{
                throw new BithubException("No se pudo crear el fileReview");
            }

        } catch (BithubException e) {
            throw new BithubException("No se pudo crear el fileReview");
        }

    }

    @Override
    public Optional<Review> getReviewById(long id) {
        //return Optional.empty();
        Review review = repository.getReviewById(id);
        return Optional.ofNullable(review);
    }

    @Override
    public List<Commit> getAllCommitsForUser(long userId) {

        return repository.getAllCommitsForUser(userId);
    }

    @Override
    public Map<Long, Long> getCommitCountByUser() {
        return repository.getCommitCountByUser();
    }

    @Override
    public List<User> getUsersThatCommittedInBranch(String branchName) throws BithubException {
        List<User> users = new ArrayList<User>();
        Branch b;
        try {
            users =repository.getUsersThatCommittedInBranch(branchName);
        } catch (BithubException e) {
            throw new BithubException("error");
        }
        return users;
    }

    @Override
    public Optional<Branch> getBranchByName(String branchName){
        Branch branch=null;
        try {
            branch=repository.getBranchByName(branchName);
        } catch (BithubException e){
            branch=null;
        }

        return Optional.ofNullable(branch);
    }



/*    @java.lang.Override
    public Optional<User> getUserByEmail(String email) {
        return null;
    }

    @java.lang.Override
    public Branch createBranch(String name) {
        Branch newBranch = new Branch();
        newBranch.setName(name);

        return newBranch;
    }

    @java.lang.Override
    public Commit createCommit(String description, String hash, User author, List<File> files, Branch branch) {
        Commit newCommit = new Commit();
        newCommit.setMessage(description);
        newCommit.setHash(hash);

        return newCommit;
    }

*/

/*
    @java.lang.Override
    public File createFile(String name, String content) {
        File archivo = new File();
        archivo.setName(name);
        archivo.setContent(content);

        return archivo;
    }



    @java.lang.Override
    public Optional<Tag> getTagByName(String tagName) {
        return null;
    }

    @java.lang.Override
    public Review createReview(Branch branch, User user) {
        return null;
    }

    @java.lang.Override
    public FileReview addFileReview(Review review, File file, int lineNumber, String comment) throws BithubException {
        return null;
    }*/


/*
    @java.lang.Override
    public List<Commit> getAllCommitsForUser(long userId) {
        return null;
    }

    @java.lang.Override
    public Map<Long, Long> getCommitCountByUser() {
        return null;
    }

    */

/*
    @java.lang.Override
    public Optional<Branch> getBranchByName(String branchName) {
        return null;
    }
    */
}
