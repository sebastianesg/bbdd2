package ar.edu.unlp.info.bd2.model;
import javax.persistence.*;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Entity
public class Branch {
    @Id
    @Column(name="id")@GeneratedValue
    public Integer id;
    @Column(name="nombre")
    public String nombre;
    @OneToMany
    public List<Commit> commits;

    public Branch(String nombre) {
        this.nombre = nombre;
        this.commits =new ArrayList<Commit>();
    }

    public void setId(Integer id){
        this.id = id;
    }

    public void setName (String name){
        nombre = name;
    }

    public Integer getId(){
        return id;
    }

    public void addCommit(Commit commit) {
        this.commits.add(commit);
    }

    public String getName(){
        return nombre;
    }

    public List<Commit> getCommits() {
        return commits;
    }
}
