package ar.edu.unlp.info.bd2.repositories;
import ar.edu.unlp.info.bd2.model.*;
import ar.edu.unlp.info.bd2.services.BithubException;
import org.hibernate.SessionFactory;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class BithubRepository {

        @Autowired
        private SessionFactory sessionFactory;

        private SessionFactory getSessionFactory() {
            return sessionFactory;
        }



        public void setSessionFactory(SessionFactory sessionFactory) {
            this.sessionFactory = sessionFactory;
        }

    @Transactional
    public void addUser(User u) {
        Session session = this.sessionFactory.getCurrentSession();
        session.save(u);
        //logger.info("User saved successfully, Person Details="+u);
    }

    @Transactional
    public void addReview(Review r) {
        Session session = this.sessionFactory.getCurrentSession();
        session.save(r);
        //logger.info("User saved successfully, Person Details="+u);
    }


    @Transactional
    public void updateUser(User u) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(u);
        //logger.info("User updated successfully, Person Details="+u);
    }
    public void updateReview(Review u) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(u);
        //logger.info("User updated successfully, Person Details="+u);
    }
    @Transactional
    public void updateBranch(Branch b) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(b);
        //logger.info("User updated successfully, Person Details="+u);
    }

    @Transactional
    public void updateCommit(Commit c) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(c);
        //logger.info("User updated successfully, Person Details="+u);
    }

    @Transactional
    public void updateFile(File f) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(f);
        //logger.info("User updated successfully, Person Details="+u);
    }
    @Transactional
    public void addBranch(Branch b) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(b);
        //logger.info("User saved successfully, Person Details="+u);
    }
    @Transactional
    public void addCommit(Commit c) {
        Session session = this.sessionFactory.getCurrentSession();
        session.save(c);
        //logger.info("User saved successfully, Person Details="+u);
    }

    @Transactional
    public void addTag(Tag t) {
        Session session = this.sessionFactory.getCurrentSession();
        session.save(t);
    }

    @Transactional
    public void addFile(File f) {
        Session session = this.sessionFactory.getCurrentSession();
        session.save(f);
        //logger.info("User saved successfully, Person Details="+u);
    }

    @Transactional
    public void addFileReview(FileReview fr) throws BithubException {
        //miramos el branch del review
        Branch unBranch = fr.getReview().getBranch();
        //obtenemos la lista de los commits del branch
        List<Commit> listaCommits = unBranch.getCommits();
        List<Boolean> blist = new ArrayList<Boolean> (listaCommits.stream().map(n->n.getFiles().contains(fr.getReviewedFile())).collect(Collectors.toSet()));


        if (!((listaCommits.stream().map(n -> n.getFiles().stream().map(m -> m.getFilename())).collect(Collectors.toList())).contains(fr.getReviewedFile().getFilename()))) {
            Session session = this.sessionFactory.getCurrentSession();
            session.save(fr);
        }
        else {
            throw new BithubException("No se encontro el archivo en el branch");
        }

        //ArrayList<String> nombreArchivos = new ArrayList<String>(listaArchivos.stream().map(n -> n.getFilename()).collect(Collectors.toList()));
        /*Collection<? extends File> listaArchivos = (Collection<? extends File>) listaCommits.stream().map(n -> n.getFiles()).collect(Collectors.toList());*/
        //new ArrayList<String>(listaCommits.stream().map(n -> n.getFiles().stream().map(m-> m.getFilename()).co(Collectors.toSet()));
        // list=new ArrayList<User>(b.getCommits().stream().map(n->n.getAuthor()).collect(Collectors.toSet()));
        //creo un arraylist como parametro le mando la lista de nombres de archivos
        // Accumulate names into a List
        //List<String> list = people.stream().map(Person::getName).collect(Collectors.toList());
        //List<String> listaNombresArchivos =  new List<String>(

        //logger.info("User saved successfully, Person Details="+u);
    }

    public Commit getCommitByHash(String hash) {
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createQuery("from Commit where hash = :hash ");
        query.setParameter("hash", hash);
        Commit c;
        try {
            query.getResultList();
            c = (Commit) query.getSingleResult();
        } catch (NoResultException e) {
            c=null;
        }
        return c;
    }

    public Tag getTagByName (String tagName) {  // throws BithubException {
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createQuery("from Tag where nombreTag = :name ");
        query.setParameter("name", tagName);
        Tag t;
        List<Tag> listaDeTags = new ArrayList<Tag>();
        listaDeTags = query.getResultList();
        if (listaDeTags.size() == 0){
            return null;
        }
        else
            return (Tag) listaDeTags.get(0);

       // try {
       //query.getResultList();

        /*} catch (NoResultException e) {
            throw new BithubException("No se pudo obtener el tag con nombre " + tagName);
        }*/
        //return t;
    }

    public Branch getBranchByName(String name) throws  BithubException{
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createQuery("from Branch where nombre = :name ");
        query.setParameter("name", name);
        Branch b;
        try {
            query.getResultList();
            b = (Branch) query.getSingleResult();
        } catch (NoResultException e) {
            throw new BithubException("error");
        }
        return b;
    }

    public Review getReviewById(long id) {
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createQuery("from Review where Id = :id ");
        query.setParameter("id", id);
        Review r;
        try {
            query.getResultList();
            r = (Review) query.getSingleResult();
        } catch (NoResultException e) {
            r = null;
        }
        return r;
    }



    public List<User> getUsersThatCommittedInBranch(String name) throws BithubException {
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createQuery("from Branch where nombre = :name ");
        query.setParameter("name", name);
        Branch b;
        List<User>list = new ArrayList<User>();
        try {
            b =(Branch) query.getSingleResult();
            list=new ArrayList<User>(b.getCommits().stream().map(n->n.getAuthor()).collect(Collectors.toSet()));
        } catch (PersistenceException e) {
            throw new BithubException("error");
        }
        return list;
    }
    public Map<Long, Long> getCommitCountByUser() {
        Session session = this.getSessionFactory().getCurrentSession();
        Query query = session.createQuery("FROM Commit c");
        Branch b;
        List<Commit>list = new ArrayList<Commit>();
        Map<Long, Long> map  = new HashMap<Long, Long>();
        try {
            list = query.getResultList();
            for (Commit commit : list) {
                if (map.containsKey(Long.valueOf(commit.getAuthor().getId()))) {
                    map.put(Long.valueOf(commit.getAuthor().getId()),Long.valueOf(2));
                }else{
                    map.put(Long.valueOf(commit.getAuthor().getId()),Long.valueOf(1));
                }
            }
        } catch (NoResultException e) {
            list= new ArrayList<Commit>();
        }
        return map;
    }
    public List<Commit> getAllCommitsForUser(long userId) {
        Session session = this.getSessionFactory().getCurrentSession();
        Long idUser=(long)userId;
        Query query = session.createQuery("from User where id = :id ");
        query.setParameter("id", idUser);
        User b;
        List<Commit> commits= new ArrayList<Commit>();
        try {
            b = (User) query.getSingleResult();
            commits=b.commits;
        } catch (NoResultException e) {
            commits = null;
        }
        return commits;
    }

}
